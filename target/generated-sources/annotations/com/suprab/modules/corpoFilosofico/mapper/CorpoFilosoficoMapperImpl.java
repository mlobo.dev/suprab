package com.suprab.modules.corpoFilosofico.mapper;

import com.suprab.modules.corpoFilosofico.dto.CorpoFilosoficoDTO;
import com.suprab.modules.corpoFilosofico.entity.CorpoFilosofico;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:23-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class CorpoFilosoficoMapperImpl implements CorpoFilosoficoMapper {

    @Override
    public CorpoFilosoficoDTO toDto(CorpoFilosofico entity) {
        if ( entity == null ) {
            return null;
        }

        CorpoFilosoficoDTO corpoFilosoficoDTO = new CorpoFilosoficoDTO();

        corpoFilosoficoDTO.setId( entity.getId() );
        corpoFilosoficoDTO.setGrau( entity.getGrau() );
        corpoFilosoficoDTO.setCorpo( entity.getCorpo() );
        corpoFilosoficoDTO.setDataGrau( entity.getDataGrau() );

        return corpoFilosoficoDTO;
    }

    @Override
    public CorpoFilosofico toEntity(CorpoFilosoficoDTO dto) {
        if ( dto == null ) {
            return null;
        }

        CorpoFilosofico corpoFilosofico = new CorpoFilosofico();

        corpoFilosofico.setId( dto.getId() );
        corpoFilosofico.setGrau( dto.getGrau() );
        corpoFilosofico.setCorpo( dto.getCorpo() );
        corpoFilosofico.setDataGrau( dto.getDataGrau() );

        return corpoFilosofico;
    }

    @Override
    public List<CorpoFilosoficoDTO> toDto(List<CorpoFilosofico> entities) {
        if ( entities == null ) {
            return null;
        }

        List<CorpoFilosoficoDTO> list = new ArrayList<CorpoFilosoficoDTO>( entities.size() );
        for ( CorpoFilosofico corpoFilosofico : entities ) {
            list.add( toDto( corpoFilosofico ) );
        }

        return list;
    }

    @Override
    public List<CorpoFilosofico> toEntity(List<CorpoFilosoficoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<CorpoFilosofico> list = new ArrayList<CorpoFilosofico>( dtos.size() );
        for ( CorpoFilosoficoDTO corpoFilosoficoDTO : dtos ) {
            list.add( toEntity( corpoFilosoficoDTO ) );
        }

        return list;
    }

    @Override
    public Set<CorpoFilosoficoDTO> toDto(Set<CorpoFilosofico> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<CorpoFilosoficoDTO> set = new HashSet<CorpoFilosoficoDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( CorpoFilosofico corpoFilosofico : entities ) {
            set.add( toDto( corpoFilosofico ) );
        }

        return set;
    }

    @Override
    public Set<CorpoFilosofico> toEntity(Set<CorpoFilosoficoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<CorpoFilosofico> set = new HashSet<CorpoFilosofico>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( CorpoFilosoficoDTO corpoFilosoficoDTO : dtos ) {
            set.add( toEntity( corpoFilosoficoDTO ) );
        }

        return set;
    }
}
