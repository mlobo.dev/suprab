package com.suprab.modules.membro.mapper;

import com.suprab.modules.corpoFilosofico.dto.CorpoFilosoficoDTO;
import com.suprab.modules.corpoFilosofico.entity.CorpoFilosofico;
import com.suprab.modules.membro.dto.MembroCadastroDTO;
import com.suprab.modules.membro.entity.Membro;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:22-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class MembroCadastroMapperImpl implements MembroCadastroMapper {

    @Override
    public MembroCadastroDTO toDto(Membro entity) {
        if ( entity == null ) {
            return null;
        }

        MembroCadastroDTO membroCadastroDTO = new MembroCadastroDTO();

        membroCadastroDTO.setId( entity.getId() );
        membroCadastroDTO.setStatus( entity.getStatus() );
        membroCadastroDTO.setCgp( entity.getCgp() );
        membroCadastroDTO.setCpf( entity.getCpf() );
        membroCadastroDTO.setNome( entity.getNome() );
        membroCadastroDTO.setTipoSanguineo( entity.getTipoSanguineo() );
        membroCadastroDTO.setCargo( entity.getCargo() );
        membroCadastroDTO.setTituloHonorifico( entity.getTituloHonorifico() );
        membroCadastroDTO.setDataNascimento( entity.getDataNascimento() );
        membroCadastroDTO.setCorposFilosoficos( corpoFilosoficoListToCorpoFilosoficoDTOList( entity.getCorposFilosoficos() ) );

        return membroCadastroDTO;
    }

    @Override
    public Membro toEntity(MembroCadastroDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Membro membro = new Membro();

        membro.setId( dto.getId() );
        membro.setStatus( dto.getStatus() );
        membro.setCgp( dto.getCgp() );
        membro.setCpf( dto.getCpf() );
        membro.setNome( dto.getNome() );
        membro.setTipoSanguineo( dto.getTipoSanguineo() );
        membro.setCargo( dto.getCargo() );
        membro.setTituloHonorifico( dto.getTituloHonorifico() );
        membro.setDataNascimento( dto.getDataNascimento() );
        membro.setCorposFilosoficos( corpoFilosoficoDTOListToCorpoFilosoficoList( dto.getCorposFilosoficos() ) );

        return membro;
    }

    @Override
    public List<MembroCadastroDTO> toDto(List<Membro> entities) {
        if ( entities == null ) {
            return null;
        }

        List<MembroCadastroDTO> list = new ArrayList<MembroCadastroDTO>( entities.size() );
        for ( Membro membro : entities ) {
            list.add( toDto( membro ) );
        }

        return list;
    }

    @Override
    public List<Membro> toEntity(List<MembroCadastroDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Membro> list = new ArrayList<Membro>( dtos.size() );
        for ( MembroCadastroDTO membroCadastroDTO : dtos ) {
            list.add( toEntity( membroCadastroDTO ) );
        }

        return list;
    }

    @Override
    public Set<MembroCadastroDTO> toDto(Set<Membro> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<MembroCadastroDTO> set = new HashSet<MembroCadastroDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( Membro membro : entities ) {
            set.add( toDto( membro ) );
        }

        return set;
    }

    @Override
    public Set<Membro> toEntity(Set<MembroCadastroDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Membro> set = new HashSet<Membro>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( MembroCadastroDTO membroCadastroDTO : dtos ) {
            set.add( toEntity( membroCadastroDTO ) );
        }

        return set;
    }

    protected CorpoFilosoficoDTO corpoFilosoficoToCorpoFilosoficoDTO(CorpoFilosofico corpoFilosofico) {
        if ( corpoFilosofico == null ) {
            return null;
        }

        CorpoFilosoficoDTO corpoFilosoficoDTO = new CorpoFilosoficoDTO();

        corpoFilosoficoDTO.setId( corpoFilosofico.getId() );
        corpoFilosoficoDTO.setGrau( corpoFilosofico.getGrau() );
        corpoFilosoficoDTO.setCorpo( corpoFilosofico.getCorpo() );
        corpoFilosoficoDTO.setDataGrau( corpoFilosofico.getDataGrau() );

        return corpoFilosoficoDTO;
    }

    protected List<CorpoFilosoficoDTO> corpoFilosoficoListToCorpoFilosoficoDTOList(List<CorpoFilosofico> list) {
        if ( list == null ) {
            return null;
        }

        List<CorpoFilosoficoDTO> list1 = new ArrayList<CorpoFilosoficoDTO>( list.size() );
        for ( CorpoFilosofico corpoFilosofico : list ) {
            list1.add( corpoFilosoficoToCorpoFilosoficoDTO( corpoFilosofico ) );
        }

        return list1;
    }

    protected CorpoFilosofico corpoFilosoficoDTOToCorpoFilosofico(CorpoFilosoficoDTO corpoFilosoficoDTO) {
        if ( corpoFilosoficoDTO == null ) {
            return null;
        }

        CorpoFilosofico corpoFilosofico = new CorpoFilosofico();

        corpoFilosofico.setId( corpoFilosoficoDTO.getId() );
        corpoFilosofico.setGrau( corpoFilosoficoDTO.getGrau() );
        corpoFilosofico.setCorpo( corpoFilosoficoDTO.getCorpo() );
        corpoFilosofico.setDataGrau( corpoFilosoficoDTO.getDataGrau() );

        return corpoFilosofico;
    }

    protected List<CorpoFilosofico> corpoFilosoficoDTOListToCorpoFilosoficoList(List<CorpoFilosoficoDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<CorpoFilosofico> list1 = new ArrayList<CorpoFilosofico>( list.size() );
        for ( CorpoFilosoficoDTO corpoFilosoficoDTO : list ) {
            list1.add( corpoFilosoficoDTOToCorpoFilosofico( corpoFilosoficoDTO ) );
        }

        return list1;
    }
}
