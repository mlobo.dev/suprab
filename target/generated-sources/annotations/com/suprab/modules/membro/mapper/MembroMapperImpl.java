package com.suprab.modules.membro.mapper;

import com.suprab.modules.corpoFilosofico.dto.CorpoFilosoficoDTO;
import com.suprab.modules.corpoFilosofico.entity.CorpoFilosofico;
import com.suprab.modules.endereco.dto.EnderecoDTO;
import com.suprab.modules.endereco.entity.Endereco;
import com.suprab.modules.membro.dto.MembroDTO;
import com.suprab.modules.membro.entity.Membro;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:23-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class MembroMapperImpl implements MembroMapper {

    @Override
    public MembroDTO toDto(Membro entity) {
        if ( entity == null ) {
            return null;
        }

        MembroDTO membroDTO = new MembroDTO();

        membroDTO.setId( entity.getId() );
        membroDTO.setStatus( entity.getStatus() );
        membroDTO.setCgp( entity.getCgp() );
        membroDTO.setCpf( entity.getCpf() );
        membroDTO.setNome( entity.getNome() );
        membroDTO.setTipoSanguineo( entity.getTipoSanguineo() );
        membroDTO.setCargo( entity.getCargo() );
        membroDTO.setTituloHonorifico( entity.getTituloHonorifico() );
        membroDTO.setDataNascimento( entity.getDataNascimento() );
        membroDTO.setEndereco( enderecoToEnderecoDTO( entity.getEndereco() ) );
        membroDTO.setCorposFilosoficos( corpoFilosoficoListToCorpoFilosoficoDTOList( entity.getCorposFilosoficos() ) );

        return membroDTO;
    }

    @Override
    public Membro toEntity(MembroDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Membro membro = new Membro();

        membro.setId( dto.getId() );
        membro.setStatus( dto.getStatus() );
        membro.setCgp( dto.getCgp() );
        membro.setCpf( dto.getCpf() );
        membro.setNome( dto.getNome() );
        membro.setTipoSanguineo( dto.getTipoSanguineo() );
        membro.setCargo( dto.getCargo() );
        membro.setTituloHonorifico( dto.getTituloHonorifico() );
        membro.setDataNascimento( dto.getDataNascimento() );
        membro.setCorposFilosoficos( corpoFilosoficoDTOListToCorpoFilosoficoList( dto.getCorposFilosoficos() ) );
        membro.setEndereco( enderecoDTOToEndereco( dto.getEndereco() ) );

        return membro;
    }

    @Override
    public List<MembroDTO> toDto(List<Membro> entities) {
        if ( entities == null ) {
            return null;
        }

        List<MembroDTO> list = new ArrayList<MembroDTO>( entities.size() );
        for ( Membro membro : entities ) {
            list.add( toDto( membro ) );
        }

        return list;
    }

    @Override
    public List<Membro> toEntity(List<MembroDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Membro> list = new ArrayList<Membro>( dtos.size() );
        for ( MembroDTO membroDTO : dtos ) {
            list.add( toEntity( membroDTO ) );
        }

        return list;
    }

    @Override
    public Set<MembroDTO> toDto(Set<Membro> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<MembroDTO> set = new HashSet<MembroDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( Membro membro : entities ) {
            set.add( toDto( membro ) );
        }

        return set;
    }

    @Override
    public Set<Membro> toEntity(Set<MembroDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Membro> set = new HashSet<Membro>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( MembroDTO membroDTO : dtos ) {
            set.add( toEntity( membroDTO ) );
        }

        return set;
    }

    protected EnderecoDTO enderecoToEnderecoDTO(Endereco endereco) {
        if ( endereco == null ) {
            return null;
        }

        EnderecoDTO enderecoDTO = new EnderecoDTO();

        enderecoDTO.setId( endereco.getId() );
        enderecoDTO.setCpf( endereco.getCpf() );
        enderecoDTO.setCep( endereco.getCep() );
        enderecoDTO.setCidade( endereco.getCidade() );
        enderecoDTO.setUf( endereco.getUf() );

        return enderecoDTO;
    }

    protected CorpoFilosoficoDTO corpoFilosoficoToCorpoFilosoficoDTO(CorpoFilosofico corpoFilosofico) {
        if ( corpoFilosofico == null ) {
            return null;
        }

        CorpoFilosoficoDTO corpoFilosoficoDTO = new CorpoFilosoficoDTO();

        corpoFilosoficoDTO.setId( corpoFilosofico.getId() );
        corpoFilosoficoDTO.setGrau( corpoFilosofico.getGrau() );
        corpoFilosoficoDTO.setCorpo( corpoFilosofico.getCorpo() );
        corpoFilosoficoDTO.setDataGrau( corpoFilosofico.getDataGrau() );

        return corpoFilosoficoDTO;
    }

    protected List<CorpoFilosoficoDTO> corpoFilosoficoListToCorpoFilosoficoDTOList(List<CorpoFilosofico> list) {
        if ( list == null ) {
            return null;
        }

        List<CorpoFilosoficoDTO> list1 = new ArrayList<CorpoFilosoficoDTO>( list.size() );
        for ( CorpoFilosofico corpoFilosofico : list ) {
            list1.add( corpoFilosoficoToCorpoFilosoficoDTO( corpoFilosofico ) );
        }

        return list1;
    }

    protected CorpoFilosofico corpoFilosoficoDTOToCorpoFilosofico(CorpoFilosoficoDTO corpoFilosoficoDTO) {
        if ( corpoFilosoficoDTO == null ) {
            return null;
        }

        CorpoFilosofico corpoFilosofico = new CorpoFilosofico();

        corpoFilosofico.setId( corpoFilosoficoDTO.getId() );
        corpoFilosofico.setGrau( corpoFilosoficoDTO.getGrau() );
        corpoFilosofico.setCorpo( corpoFilosoficoDTO.getCorpo() );
        corpoFilosofico.setDataGrau( corpoFilosoficoDTO.getDataGrau() );

        return corpoFilosofico;
    }

    protected List<CorpoFilosofico> corpoFilosoficoDTOListToCorpoFilosoficoList(List<CorpoFilosoficoDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<CorpoFilosofico> list1 = new ArrayList<CorpoFilosofico>( list.size() );
        for ( CorpoFilosoficoDTO corpoFilosoficoDTO : list ) {
            list1.add( corpoFilosoficoDTOToCorpoFilosofico( corpoFilosoficoDTO ) );
        }

        return list1;
    }

    protected Endereco enderecoDTOToEndereco(EnderecoDTO enderecoDTO) {
        if ( enderecoDTO == null ) {
            return null;
        }

        Endereco endereco = new Endereco();

        endereco.setId( enderecoDTO.getId() );
        endereco.setCpf( enderecoDTO.getCpf() );
        endereco.setCidade( enderecoDTO.getCidade() );
        endereco.setUf( enderecoDTO.getUf() );
        endereco.setCep( enderecoDTO.getCep() );

        return endereco;
    }
}
