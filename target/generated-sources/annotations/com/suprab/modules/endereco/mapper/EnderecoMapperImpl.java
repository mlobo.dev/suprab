package com.suprab.modules.endereco.mapper;

import com.suprab.modules.endereco.dto.EnderecoDTO;
import com.suprab.modules.endereco.entity.Endereco;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:22-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class EnderecoMapperImpl implements EnderecoMapper {

    @Override
    public EnderecoDTO toDto(Endereco entity) {
        if ( entity == null ) {
            return null;
        }

        EnderecoDTO enderecoDTO = new EnderecoDTO();

        enderecoDTO.setId( entity.getId() );
        enderecoDTO.setCpf( entity.getCpf() );
        enderecoDTO.setCep( entity.getCep() );
        enderecoDTO.setCidade( entity.getCidade() );
        enderecoDTO.setUf( entity.getUf() );

        return enderecoDTO;
    }

    @Override
    public Endereco toEntity(EnderecoDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Endereco endereco = new Endereco();

        endereco.setId( dto.getId() );
        endereco.setCpf( dto.getCpf() );
        endereco.setCidade( dto.getCidade() );
        endereco.setUf( dto.getUf() );
        endereco.setCep( dto.getCep() );

        return endereco;
    }

    @Override
    public List<EnderecoDTO> toDto(List<Endereco> entities) {
        if ( entities == null ) {
            return null;
        }

        List<EnderecoDTO> list = new ArrayList<EnderecoDTO>( entities.size() );
        for ( Endereco endereco : entities ) {
            list.add( toDto( endereco ) );
        }

        return list;
    }

    @Override
    public List<Endereco> toEntity(List<EnderecoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Endereco> list = new ArrayList<Endereco>( dtos.size() );
        for ( EnderecoDTO enderecoDTO : dtos ) {
            list.add( toEntity( enderecoDTO ) );
        }

        return list;
    }

    @Override
    public Set<EnderecoDTO> toDto(Set<Endereco> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<EnderecoDTO> set = new HashSet<EnderecoDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( Endereco endereco : entities ) {
            set.add( toDto( endereco ) );
        }

        return set;
    }

    @Override
    public Set<Endereco> toEntity(Set<EnderecoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Endereco> set = new HashSet<Endereco>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( EnderecoDTO enderecoDTO : dtos ) {
            set.add( toEntity( enderecoDTO ) );
        }

        return set;
    }
}
