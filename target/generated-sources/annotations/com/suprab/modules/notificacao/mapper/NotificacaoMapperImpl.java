package com.suprab.modules.notificacao.mapper;

import com.suprab.modules.notificacao.dto.NotificacaoDTO;
import com.suprab.modules.notificacao.entity.Notificacao;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:22-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class NotificacaoMapperImpl implements NotificacaoMapper {

    @Override
    public NotificacaoDTO toDto(Notificacao entity) {
        if ( entity == null ) {
            return null;
        }

        NotificacaoDTO notificacaoDTO = new NotificacaoDTO();

        notificacaoDTO.setId( entity.getId() );
        notificacaoDTO.setTitulo( entity.getTitulo() );
        notificacaoDTO.setMensagem( entity.getMensagem() );

        return notificacaoDTO;
    }

    @Override
    public Notificacao toEntity(NotificacaoDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Notificacao notificacao = new Notificacao();

        notificacao.setId( dto.getId() );
        notificacao.setTitulo( dto.getTitulo() );
        notificacao.setMensagem( dto.getMensagem() );

        return notificacao;
    }

    @Override
    public List<NotificacaoDTO> toDto(List<Notificacao> entities) {
        if ( entities == null ) {
            return null;
        }

        List<NotificacaoDTO> list = new ArrayList<NotificacaoDTO>( entities.size() );
        for ( Notificacao notificacao : entities ) {
            list.add( toDto( notificacao ) );
        }

        return list;
    }

    @Override
    public List<Notificacao> toEntity(List<NotificacaoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Notificacao> list = new ArrayList<Notificacao>( dtos.size() );
        for ( NotificacaoDTO notificacaoDTO : dtos ) {
            list.add( toEntity( notificacaoDTO ) );
        }

        return list;
    }

    @Override
    public Set<NotificacaoDTO> toDto(Set<Notificacao> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<NotificacaoDTO> set = new HashSet<NotificacaoDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( Notificacao notificacao : entities ) {
            set.add( toDto( notificacao ) );
        }

        return set;
    }

    @Override
    public Set<Notificacao> toEntity(Set<NotificacaoDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Notificacao> set = new HashSet<Notificacao>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( NotificacaoDTO notificacaoDTO : dtos ) {
            set.add( toEntity( notificacaoDTO ) );
        }

        return set;
    }
}
