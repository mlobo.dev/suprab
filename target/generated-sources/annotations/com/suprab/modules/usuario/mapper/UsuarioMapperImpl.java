package com.suprab.modules.usuario.mapper;

import com.suprab.modules.usuario.dto.UsuarioDTO;
import com.suprab.modules.usuario.entity.Usuario;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T13:56:22-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9 (Ubuntu)"
)
@Component
public class UsuarioMapperImpl implements UsuarioMapper {

    @Override
    public UsuarioDTO toDto(Usuario entity) {
        if ( entity == null ) {
            return null;
        }

        UsuarioDTO usuarioDTO = new UsuarioDTO();

        usuarioDTO.setId( entity.getId() );
        usuarioDTO.setNome( entity.getNome() );
        usuarioDTO.setCpf( entity.getCpf() );
        usuarioDTO.setSenha( entity.getSenha() );
        usuarioDTO.setLogin( entity.getLogin() );

        return usuarioDTO;
    }

    @Override
    public Usuario toEntity(UsuarioDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Usuario usuario = new Usuario();

        usuario.setId( dto.getId() );
        usuario.setNome( dto.getNome() );
        usuario.setCpf( dto.getCpf() );
        usuario.setSenha( dto.getSenha() );
        usuario.setLogin( dto.getLogin() );

        return usuario;
    }

    @Override
    public List<UsuarioDTO> toDto(List<Usuario> entities) {
        if ( entities == null ) {
            return null;
        }

        List<UsuarioDTO> list = new ArrayList<UsuarioDTO>( entities.size() );
        for ( Usuario usuario : entities ) {
            list.add( toDto( usuario ) );
        }

        return list;
    }

    @Override
    public List<Usuario> toEntity(List<UsuarioDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Usuario> list = new ArrayList<Usuario>( dtos.size() );
        for ( UsuarioDTO usuarioDTO : dtos ) {
            list.add( toEntity( usuarioDTO ) );
        }

        return list;
    }

    @Override
    public Set<UsuarioDTO> toDto(Set<Usuario> entities) {
        if ( entities == null ) {
            return null;
        }

        Set<UsuarioDTO> set = new HashSet<UsuarioDTO>( Math.max( (int) ( entities.size() / .75f ) + 1, 16 ) );
        for ( Usuario usuario : entities ) {
            set.add( toDto( usuario ) );
        }

        return set;
    }

    @Override
    public Set<Usuario> toEntity(Set<UsuarioDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Usuario> set = new HashSet<Usuario>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( UsuarioDTO usuarioDTO : dtos ) {
            set.add( toEntity( usuarioDTO ) );
        }

        return set;
    }
}
